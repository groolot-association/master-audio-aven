CD_DA
CD_TEXT {
  LANGUAGE_MAP {
    0 : EN
  }
  LANGUAGE 0 {
    TITLE "Aven"
    PERFORMER ""
  }
}

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "vapeur"
     PERFORMER ""
  }
}
FILE "2011-Aven-recueil_musical-Gregory_DAVID-Licence_Art_Libre.wav"  00:00:00 05:48:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "des papillons dans le ventre"
     PERFORMER ""
  }
}
FILE "2011-Aven-recueil_musical-Gregory_DAVID-Licence_Art_Libre.wav"  05:48:00 09:57:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "gorgonbasse"
     PERFORMER ""
  }
}
FILE "2011-Aven-recueil_musical-Gregory_DAVID-Licence_Art_Libre.wav"  15:45:00 10:27:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "Sigmund Freud"
     PERFORMER ""
  }
}
FILE "2011-Aven-recueil_musical-Gregory_DAVID-Licence_Art_Libre.wav"  26:12:00 01:36:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "aven"
     PERFORMER ""
  }
}
FILE "2011-Aven-recueil_musical-Gregory_DAVID-Licence_Art_Libre.wav"  27:48:00 07:36:00
START 00:00:00

TRACK AUDIO
COPY
NO PRE_EMPHASIS
CD_TEXT {
  LANGUAGE 0 {
     TITLE "en attendant la pluie"
     PERFORMER ""
  }
}
FILE "2011-Aven-recueil_musical-Gregory_DAVID-Licence_Art_Libre.wav"  35:24:00 08:12:02
START 00:00:00
